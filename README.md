# Danh sách các thuật toán trong nghành công nghệ thông tin 

### List of all Algorithms in IT:
[List of algorithms](https://en.wikipedia.org/wiki/List_of_algorithms)

### Sơ đồ mạng và tìm đường Gant

+ Part 1: https://youtu.be/5x4CKsMqMdU

+ Part 2: https://youtu.be/EYY_1KdjpU0

+ Part 3: https://youtu.be/JFnDr5AZFRE

+ Part 4: https://youtu.be/K1M4nZdw9Vs

### Độ Phức Tạp Của Thuật Toán
### Thiết Kế Giải Thuật 
### Cấu Trúc Dữ Liệu Và Giải Thuật
### Các Thuật Toán Sắp Xếp 

+ Linear Search
+ Binary Search
+ Ternary Search

### Các Thuật Toán Tìm Kiếm

+ Bubble Sort
+ Selection Sort
+ Insertion Sort
+ Merge Sort
+ Quick Sort
+ Counting Sort
+ Radix Sort
+ Heap Sort
+ Bucket Sort
+ Giải thuật tìm kiếm A*

### Đệ Quy Quay Lui
### Các Thao Tác Cây Tìm Kiếm & Cây AVL
### Cây Đỏ Đen
### B Tree 
### Thuật Toán Chuỗi
+ Khái niệm cơ bản của Thao tác chuỗi (asics of String Manipulation)
+ Tìm kiếm Chuỗi (String Searching)
+ Thuật toán Z (Z Algorithm)
+ Thuật toán của Manachar (Manachar’s Algorithm)

### Thuật toán tham lam
+ Basics of Greedy Algorithms

### Đồ Thị 
+ Biểu diễn đồ thị (Graph Representation)

+ Tìm Kiếm Theo Chiều Rộng (Breadth First Search)

+ Tìm Kiếm Theo Chiều Sâu (Depth First Search)

+ Cây Bao Trùm Nhỏ Nhất (Minimum Spanning Tree)

+ Thuật Toán Đường Đi Ngắn Nhất (Shortest Path Algorithms)

+ Thuật toán Flood-fill (Flood-fill Algorithm)

+ Đồ Thị Liên Thông (Articulation Points and Bridges)

+ Biconnected Components 

+ Strongly Connected Components
+ Topological Sort
+ Hamiltonian Path
+ Maximum flow
+ Minimum Cost Maximum Flow
+ Min-cut

### Dynamic Programming 
+ Introduction to Dynamic Programming 1
+ 2 Dimensional
+ State space reduction
+ Dynamic Programming and Bit Masking


# Cấu Trúc Dữ Liệu


### Arrays
+ 1-D
+ Multi-dimensional

### Stacks
+ Basics of Stacks

### Queues
+ Basics of Queues

### Hash Tables
+ Basics of Hash Tables

### Linked List
+ Singly Linked List

### Trees
+ Binary/ N-ary Trees
+ Binary Search Tree
+ Heaps/Priority Queues


### Advanced Data Structures
+ Trie (Keyword Tree)
+ Segment Trees
+ Fenwick (Binary Indexed) Trees
+ Suffix Trees
+ Suffix Arrays


### Disjoint Data Structures
+ Basics of Disjoint Data Structures

### [Thuật toán Christofides](https://vi.wikipedia.org/wiki/Thu%E1%BA%ADt_to%C3%A1n_Christofides)

### [Thuật toán trực tuyến](https://vi.wikipedia.org/wiki/Thu%E1%BA%ADt_to%C3%A1n_tr%E1%BB%B1c_tuy%E1%BA%BFn)

### [Thuật toán Dinitz](https://vi.wikipedia.org/wiki/Thu%E1%BA%ADt_to%C3%A1n_Dinitz)

### [Thuật toán bình phương và nhân](https://vi.wikipedia.org/wiki/Thu%E1%BA%ADt_to%C3%A1n_b%C3%ACnh_ph%C6%B0%C6%A1ng_v%C3%A0_nh%C3%A2n)

### [Thuật toán luật A](https://vi.wikipedia.org/wiki/Thu%E1%BA%ADt_to%C3%A1n_lu%E1%BA%ADt_A)

### [Thuật toán Soundex](https://vi.wikipedia.org/wiki/Soundex)

### [Thuật toán Chudnovsky](https://vi.wikipedia.org/wiki/Thu%E1%BA%ADt_to%C3%A1n_Chudnovsky)

### [Thuật toán hình học](https://vi.wikipedia.org/wiki/Th%E1%BB%83_lo%E1%BA%A1i:Thu%E1%BA%ADt_to%C3%A1n_h%C3%ACnh_h%E1%BB%8Dc)

[Thuật Toán Wiki](https://vi.wikipedia.org/wiki/Th%E1%BB%83_lo%E1%BA%A1i:Thu%E1%BA%ADt_to%C3%A1n)

Tham khảo 1: [https://www.hackerearth.com/practice](https://www.hackerearth.com/practice/)

Tham khảo 2: [https://www.geeksforgeeks.org/](https://www.geeksforgeeks.org/)

Tham khảo 3: [http://vnoi.info/wiki/Home](http://vnoi.info/wiki/Home)

Tham khảo 4: [https://www.codechef.com](https://www.codechef.com)

Tham khảo 5: [https://www.hackerearth.com/](https://www.hackerearth.com/)

Tham khảo 6: [http://www.spoj.com](http://www.spoj.com)

Tham khảo 7: [http://codeforces.com](http://codeforces.com)

### 1. Về phần Data Structures trong trường sẽ dạy các bạn Array, Linked List, Stack, Queue, Hash Tables...Bạn nên học thêm:

- Pair, Set, Multiset, Map, Multimap: Đây là các cấu trúc thuộc thư viện Standard Template Library (STL) biết thêm những cấu trúc này giúp bạn có thêm công cụ để giải các bài toán từ đơn giản đến phức tạp.

Tài liệu tham khảo: [http://www.cplusplus.com/forum/general/68770/](http://www.cplusplus.com/forum/general/68770/)

- Priority Queue - Heap (Hàng đợi ưu tiên): cũng áp dụng quy trình FIFO, ai tới trước thì ra trước. Nhưng đối với hàng đợi ưu tiên, tới trước chưa chắc sẽ ra trước, nó còn phụ thuộc vào độ ưu tiên của các phần tử trong hàng đợi.

Tài liệu tham khảo: [http://pages.cs.wisc.edu/~vernon/cs367/notes/11.PRIORITY-Q.html](http://pages.cs.wisc.edu/~vernon/cs367/notes/11.PRIORITY-Q.html)

- Suffix Array (Mảng hậu tố): là những cấu trúc dữ liệu cực kì quan trọng trong việc giải các bài toán liên quan đến chuỗi.

Tài liệu tham khảo: [http://web.stanford.edu/class/cs97si/suffix-array.pdf](http://web.stanford.edu/class/cs97si/suffix-array.pdf)

- Trie (Cây tiền tố): Đây cũng là một cấu trúc dữ liệu giúp bạn giải quyết các bài toán liên quan đến chuỗi.

Tài liệu tham khảo: [https://www.topcoder.com/community/data-science/data-science-tutorials/using-tries/](https://www.topcoder.com/community/data-science/data-science-tutorials/using-tries/)

### 2. Về Graph (đồ thị) trong trường sẽ dạy cho bạn DFS, BFS, Dijkstra, Prim,...Bạn cần phải học thêm 2 thuật toán sau:

- Topological Sort: Sắp xếp trên đồ thị giải quyết các bài toán liên quan đến lập kế hoạch cho các công việc hoặc dự án.

Tài liệu tham khảo: [https://www.cs.usfca.edu/~galles/visualization/TopoSortDFS.html](https://www.cs.usfca.edu/~galles/visualization/TopoSortDFS.html)

- Johnson’s algorithm: Giải quyết các bài toán tương tự như  Floyd Warshall tìm đường đi ngắn nhất giữa tất cả các cặp đỉnh.

Tài liệu tham khảo: [http://www.geeksforgeeks.org/johnsons-algorithm/](http://www.geeksforgeeks.org/johnsons-algorithm/)

### 3. Về sắp xếp và tìm kiếm (Sorting And Searching) trong trường sẽ dạy các bạn (Quicksort, Heapsort...) và (Binary Search). Bạn cần học thêm:

- Interpolation Search (Tìm kiếm nội suy): Một biến thể của tìm kiếm nhị phân, tuy nhiên nó còn mạnh hơn cả tìm kiếm nhị phân.

Tài liệu tham khảo: [https://www.tutorialspoint.com/data_structures_algorithms/interpolation_search_algorithm.htm](https://www.tutorialspoint.com/data_structures_algorithms/interpolation_search_algorithm.htm)

- Aho Corasick String Matching: Giải quyết bài toán tìm kiếm đối sánh chuỗi đa mẫu.

Tài liệu tham khảo: [http://blog.ivank.net/aho-corasick-algorithm-in-as3.html](http://blog.ivank.net/aho-corasick-algorithm-in-as3.html)

### 4. Về quy hoạch động (Dynamic Programming) đây là phần rất khó, có trường sẽ dạy các bạn 1 ít phần cơ bản như Knapsack (Bài toán ba lô), Longest Increasing Subsequence...Thậm chí có trường phần này sẽ vào "bài học thêm". Tuy nhiên mình nói thật với các bạn rằng học tốt quy hoạch động là một lợi thế rất lớn dù bạn làm trong bất cứ chuyên ngành nào của CNTT. Kiến thức quy hoạch động là vô cùng lớn và khó học, mình xin tạm liệt kê các phần hay gặp nhất mà bạn cần thiết thêm:

- Edit Distance: hay còn gọi là Levenshtein distance, giải quyết bài toán biến chuỗi này thành chuỗi kia.

Tài liệu tham khảo: [https://web.stanford.edu/class/cs124/lec/med.pdf](https://web.stanford.edu/class/cs124/lec/med.pdf)

- Matrix chain multiplication (phép nhân ma trận theo chuỗi).

Tài liệu tham khảo: [http://www.radford.edu/~nokie/classes/360/dp-matrix-parens.html](http://www.radford.edu/~nokie/classes/360/dp-matrix-parens.html)

- Minimum Partition: Xử lý trong việc chia mảng ra thành 2 phần khác nhau sao cho hiệu của 2 phần là nhỏ nhất.

Tài liệu tham khảo: [http://algorithmsandme.in/2014/04/balanced-partition-problem/](http://algorithmsandme.in/2014/04/balanced-partition-problem/)

- Nếu có thời gian bạn nên đầu tư vào một số bài toán quy hoạch động nâng cao, phần này rất khó và bạn phải tốn rất nhiều thời gian để hiểu và sử dụng thuần thục:  DP tree,  DP BIT/Segment tree, DP bitmask ,  DP Convex hull.

### 5. Về Network Flow (luồng) trong trường sẽ dạy các bạn Hopcroft–Karp, Maxflow Ford Furkerson. Bạn cần học thêm:

- Minimum cut (Lát cắt cực tiểu): Phần này trong trường có thể sẽ nói sơ qua cho bạn về lý thuyết, bạn cần làm thêm bài tập vào. Vì mình thấy nhiều bạn có học qua phần này nhưng hầu như hỏi đến đều không hiểu hoặc không biết gì, các bạn nên xem thêm tài liệu bên dưới.

Tài liệu tham khảo: [http://riot.ieor.berkeley.edu/Applications/WeightedMinCut/](http://riot.ieor.berkeley.edu/Applications/WeightedMinCut/)

- Dinic’s algorithm: Cũng giải quyết bài toán tìm luồng cực tại trong mạng.

Tài liệu tham khảo: [http://www.cse.unt.edu/~tarau/teaching/AnAlgo/Dinic's%20algorithm.pdf](http://www.cse.unt.edu/~tarau/teaching/AnAlgo/Dinic's%20algorithm.pdf)

### 6. Về tree (cây) trong trường sẽ dạy các bạn Binary Search Tree, AVL Tree, Red Black Tree...Bạn cần học thêm:

- K Dimensional Tree: Lưu trữ và quản lý các điểm thuộc không gian K-chiều.

Tài liệu tham khảo: [http://www.geeksforgeeks.org/k-dimensional-tree/](http://www.geeksforgeeks.org/k-dimensional-tree/)